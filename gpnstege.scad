$fs= 0.5;

 
//npgcube();
//npgcubeSupports();
//pngcube();

//npgcubeAdd();
//gpnedgealigned();
//gpncubealigned();
gnpcubealigned();

font = "Heysei Synthesizer:style=Regular";

module invertedcube(){
difference(){
    cube(22);
    translate([1+10,1.5+9,-1])linear_extrude(height = 24) {
       text(text = str("G"), font = font, size = 20, halign = a[1],halign="center", valign="center");
     }
     translate([-1,1.5+9,11.5])rotate([0,90,0])linear_extrude(height = 24) {
       text(text = str("P"), font = font, size = 20, halign = a[1],halign="center", valign="center");
     }
     translate([10,-1,11])rotate([-90,0,0])linear_extrude(height = 24) {
       text(text = str("N"), font = font, size = 20, halign = a[1],halign="center", valign="center");
     }
}
}


module pngcube(){
translate([-11,-11,-11]){
     intersection(){
     translate([1+10,1.5+9,-1])linear_extrude(height = 24) {
       text(text = str("P"), font = font, size = 20, halign = a[1],halign="center", valign="center");
     }
     translate([-1,1.5+9,11.5])rotate([0,90,0])linear_extrude(height = 24) {
       text(text = str("N"), font = font, size = 20, halign = a[1],halign="center", valign="center");
     }
     translate([10,-1,11])rotate([-90,0,0])linear_extrude(height = 24) {
       text(text = str("G"), font = font, size = 20, halign = a[1],halign="center", valign="center");
     }
 }
     
 }
 }
 
 
 
 module npgcube(){
 translate([-11,-11,-11]){
     intersection(){
     translate([1+10,1.5+9,-1])linear_extrude(height = 24) {
       text(text = str("N"), font = font, size = 20, halign = a[1],halign="center", valign="center");
     }
     translate([-1,1.5+9,11.5])rotate([0,90,0])linear_extrude(height = 24) {
       text(text = str("P"), font = font, size = 20, halign = a[1],halign="center", valign="center");
     }
     translate([10,-1,11])rotate([-90,0,0])linear_extrude(height = 24) {
       text(text = str("G"), font = font, size = 20, halign = a[1],halign="center", valign="center");
     }
 }
     
     
 }
 
 }
 
  module npgcubeAdd(){
 translate([-11,-11,-11]){
     extrudeheight=2;
     translate([1+10,1.5+9,-1])linear_extrude(height = extrudeheight) {
       text(text = str("N"), font = font, size = 20, halign = a[1],halign="center", valign="center");
     }
     translate([-1,1.5+9,11.5])rotate([0,90,0])linear_extrude(height = extrudeheight) {
       text(text = str("P"), font = font, size = 20, halign = a[1],halign="center", valign="center");
     }
     translate([10,-1,11])rotate([-90,0,0])linear_extrude(height = extrudeheight) {
       text(text = str("G"), font = font, size = 20, halign = a[1],halign="center", valign="center");
     }   
 }
 
 }
 
 module gpnedgealigned(){
 translate([0,5,0])rotate([90,0,0])mirror(0,0,1)scale(0.43478)scale([1,1.15,1])translate([-0.3,-0.25,0]) linear_extrude(height = 1) { text(text = str("N"), font = font, size = 20, halign = a[1],halign="center", valign="center");}
       
        translate([0,0,5])rotate([180,0,0])mirror(0,0,1)scale(0.43478)scale([1,1.15,1])translate([-0.3,-0.25,0]) linear_extrude(height = 1) {
       text(text = str("G"), font = font, size = 20, halign = a[1],halign="center", valign="center");}
       
        translate([5,0,0])rotate([0,0,-90])rotate([90,0,0])mirror(0,0,1)scale(0.43478)scale([1,1.15,1])translate([-0.3,-0.25,0]) linear_extrude(height = 1) { text(text = str("P"), font = font, size = 20, halign = a[1],halign="center", valign="center");}
    }
    
    
module gpncubealigned() {
    intersection(){
        translate([0,5,0])rotate([90,0,0])mirror(0,0,1)scale(0.43478)scale([1,1.15,1])translate([-0.3,-0.25,0]) linear_extrude(height = 23) { 
            text(text = str("N"), font = font, size = 20, halign = a[1],halign="center", valign="center");
        }
       
        translate([0,0,5])rotate([180,0,0])mirror(0,0,1)scale(0.43478)scale([1,1.15,1])translate([-0.3,-0.25,0]) linear_extrude(height = 23) {
            text(text = str("G"), font = font, size = 20, halign = a[1],halign="center", valign="center");
        }
       
        translate([5,0,0])rotate([0,0,-90])rotate([90,0,0])mirror(0,0,1)scale(0.43478)scale([1,1.15,1])translate([-0.3,-0.25,0]) linear_extrude(height = 23) { 
            text(text = str("P"), font = font, size = 20, halign = a[1],halign="center", valign="center");
        }
    }
}

module gnpcubealigned() {
    intersection(){
        translate([0,5,0])rotate([90,0,0])mirror(0,0,1)scale(0.43478)scale([1,1.15,1])translate([-0.3,-0.25,0]) linear_extrude(height = 23) { 
            text(text = str("P"), font = font, size = 20, halign = a[1],halign="center", valign="center");
        }
       
        translate([0,0,5])rotate([180,0,0])mirror(0,0,1)scale(0.43478)scale([1,1.15,1])translate([-0.3,-0.25,0]) linear_extrude(height = 23) {
            text(text = str("G"), font = font, size = 20, halign = a[1],halign="center", valign="center");
        }
       
        translate([5,0,0])rotate([0,0,-90])rotate([90,0,0])mirror(0,0,1)scale(0.43478)scale([1,1.15,1])translate([-0.3,-0.25,0]) linear_extrude(height = 23) { 
            text(text = str("N"), font = font, size = 20, halign = a[1],halign="center", valign="center");
        }
    }
}
        
 
 module npgcubeSupports(){
translate([-1.3,0,0])rotate([-90,0,0])cylinder(d=1,h=9.75);
 
 translate([7,0,0])rotate([-90,0,0])cylinder(d=1,h=9.75);
     
 translate([-6,8.75,-8])cube([15,1,1]);
 translate([7,8.75,-8])cube([1,1,15]);
 }